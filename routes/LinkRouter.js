const router = require("express").Router();
const verify = require("./TokenVerif");

const linkController = require("../controllers/LinkController");


router.post("/generate", linkController.generateUrl)

router.get("/redirect/:id", linkController.redirect)





module.exports = router;
