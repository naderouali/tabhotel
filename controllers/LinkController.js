const router = require("express").Router();
let User = require("../models/UserModel");
let Link = require("../models/LinkModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const shortid = require('shortid');
var anyBase = require('any-base')
var hexTo62 = anyBase(anyBase.HEX, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")





const generateUrl = async (req, res, next) => {

    try {

        const real = req.body.real;
        const owner = req.body.owner;

        const shortID = shortid.generate();

        const short = "http://localhost:5000/api/redirect/" + shortID;

        const newLink = new Link({ real, short, owner });

        newLink
            .save()
            .then(() => {

                console.log('success')

                res.json({
                    success: true,
                    newLink: newLink._id
                })
            })
            .catch((err) => {
                console.log(err)
                res.json({
                    success: false,
                    message: "mongoose-error"
                })
            });
    } catch (error) {
        return res.json({
            success: false,
            message: "server-error"
        })
    }
}





const redirect = async (req, res, next) => {

    try {
        const id = req.params.id;
        console.log(id);


        Link.findOne({ _id: id })
            .then((li) => {
                
            
                console.log(li.real)
                
                res.redirect(li.real)
                

            }).catch(err => {
                console.log(err)
                return res.json({
                    success: false,
                    message: "mongoose-error"
                })
            })


    } catch (error) {
        console.log(error)
        return res.json({
            success: false,
            message: "server-error"
        })
    }

}




module.exports = {
    generateUrl,
    redirect
}