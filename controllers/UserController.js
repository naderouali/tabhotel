const router = require("express").Router();
let User = require("../models/UserModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");

var SibApiV3Sdk = require('sib-api-v3-sdk');
var defaultClient = SibApiV3Sdk.ApiClient.instance;

var anyBase = require('any-base')
var hexTo62 = anyBase(anyBase.HEX, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")




const register = async (req, res, next) => {

    try {
        console.log(req.body);
        const emailExist = await User.findOne({ email: req.body.email });
        if (emailExist)
            return res.json({
                success: false,
                message: "mail-exist"
            })

        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        const username = req.body.username;
        const email = req.body.email;
        const password = hashedPassword;

        const newUser = new User({ username, email, password });

        newUser
            .save()
            .then(() => {
                
                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                      user: 'oualinader@gmail.com',
                      pass: 'ipoccsnzakrmtfyy'
                    }
                  });
                  
                  var mailOptions = {
                    from: 'tabhotel@gmail.com',
                    to: email,
                    subject: 'Confirmation email',
                    text: 'Cliquez sur ce lien pour activer votre compte ZZZZ'
                  };
                  
                  transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                      console.log(error);
                    } else {
                      console.log('Email sent: ' + info.response);
                    }
                  });

                res.json({
                    success: true,
                    newUser: newUser._id,
                    message: "email sent"
                })
            })
            .catch((err) => {
                console.log(err)
                res.json({
                    success: false,
                    message: "mongoose-error"
                })
            });
    } catch (error) {
        return res.json({
            success: false,
            message: "server-error"
        })
    }
}


const login = async (req, res, next) => {

    try {

        console.log(req.body.email)

        const user = await User.findOne({ $or: [{ email: req.body.email }, { userName: req.body.email }] })
        // .populate({
        //     path: 'cart',
        //     populate: {
        //         path: 'owner'
        //     }
        // })

        console.log(user)

        if (!user) return res.json({
            success: false,
            message: "user-not-found"
        })

        const validPassword = await bcrypt.compare(req.body.password, user.password);

        if (!validPassword) return res.json({
            success: false,
            message: "wrong-password"
        })

        else {
            //create and assign a jwt
            const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET, { expiresIn: 2400 * 60 * 60 });
            console.log(token);
            res.json({
                success: true,
                token,
                user
            });
        }
    } catch (error) {
        return res.json({
            success: false,
            message: "server-error"
        })
    }

}




const verifyAccount = async (req, res, next) => {

    const idUser = req.body.id;


    console.log(idUser);
    // console.log(newPassword);

    const filter = { _id: idUser };
    const update = { isActive: true };

    await User.findOneAndUpdate(filter, update, {
        // new: true
    })
        .then(() => res.json({
            success: true,
            message: "verified-updated"
        }))
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "mongoose-error"
            })
        });

}





const profile = async (req, res, next) => {

    try {
        const id = req.user._id;
        const user = await User.findOne({ _id: id })


        if (!user) return res.json({
            success: false,
            message: "user-not-found"
        })

        return res.json({
            success: true,
            user: user
        })
    } catch (error) {
        return res.json({
            success: false,
            message: "server-error"
        })
    }

}


const readUser = async (req, res, next) => {

}






module.exports = {
    register,
    login,
    verifyAccount,
    profile,
    readUser,
}