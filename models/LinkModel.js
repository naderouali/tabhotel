const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const linkSchema = new Schema(
    {
        real: {
            type: String,
            trim: true,
        },
        short: {
            type: String,
            trim: true,
        },
        owner: {
            type: [{ type: mongoose.Types.ObjectId, ref: 'User' }],
        },
    },
    { timestamps: true }
);

const Link = mongoose.model("Link", linkSchema);

module.exports = Link;
